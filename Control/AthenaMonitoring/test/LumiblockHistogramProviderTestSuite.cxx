/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#undef NDEBUG
#include <cassert>
#include <iostream>
#include <list>
#include <functional>
#include <memory>

#include "TestTools/initGaudi.h"
#include "TestTools/expect.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ITHistSvc.h"
#include "AthenaKernel/getMessageSvc.h"

#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TProfile2D.h"

#include "AthenaMonitoring/HistogramDef.h"

#include "mocks/MockGenericMonitoringTool.h"
#include "mocks/MockHistogramFactory.h"

#include "../src/HistogramFiller/LumiblockHistogramProvider.h"
#include "../src/HistogramFiller/OfflineHistogramProvider.h"

using namespace std;
using namespace Monitored;

#define REGISTER_TEST_CASE(TEST_CASE_NAME) registerTestCase(&LumiblockHistogramProviderTestSuite::TEST_CASE_NAME, #TEST_CASE_NAME)

class LumiblockHistogramProviderTestSuite {
  // ==================== All registered test cases ====================
  private:
    list<function<void(void)>> registeredTestCases() {
      return {
        REGISTER_TEST_CASE(test_shouldThrowExceptionWhen_kLBNHistoryDepth_isNotDefined),
        REGISTER_TEST_CASE(test_shouldThrowExceptionWhen_kLBNHistoryDepth_isDefinedAs_NaN),
        REGISTER_TEST_CASE(test_shouldNotThrowExceptionWhen_kLBNHistoryDepth_isDefinedAsNumber),
        REGISTER_TEST_CASE(test_shouldCreateNewHistogramWithUpdatedAlias),
        REGISTER_TEST_CASE(test_shouldCreateNewHistogramWithUpdatedLumiBlock),
      };
    }

  // ==================== Test code ====================
  private:
    void beforeEach() {
      m_gmTool.reset(new MockGenericMonitoringTool());
      m_histogramFactory.reset(new MockHistogramFactory());
    }

    void afterEach() {
    }

    void test_shouldThrowExceptionWhen_kLBNHistoryDepth_isNotDefined() {
      try {
        HistogramDef histogramDef;
        LumiblockHistogramProvider testObj(m_gmTool.get(), m_histogramFactory, histogramDef);
      } catch (HistogramException&) {
        return;
      }
      assert(false);
    }

    void test_shouldThrowExceptionWhen_kLBNHistoryDepth_isDefinedAs_NaN() {
      try {
        HistogramDef histogramDef;
        histogramDef.opt = "kLBNHistoryDepth=abc";
        LumiblockHistogramProvider testObj(m_gmTool.get(), m_histogramFactory, histogramDef);
      } catch (HistogramException&) {
        return;
      }
      assert(false);
    }

    void test_shouldNotThrowExceptionWhen_kLBNHistoryDepth_isDefinedAsNumber() {
      HistogramDef histogramDef;
      histogramDef.opt = "kLBNHistoryDepth=12345";
      LumiblockHistogramProvider testObj(m_gmTool.get(), m_histogramFactory, histogramDef);
    }

    void test_shouldCreateNewHistogramWithUpdatedAlias() {
      auto expectedFlow = {
        make_tuple(0, "test alias(0-2)"), 
        make_tuple(1, "test alias(0-2)"),
        make_tuple(2, "test alias(0-2)"),
        make_tuple(3, "test alias(3-5)"),
        make_tuple(4, "test alias(3-5)"),
        make_tuple(5, "test alias(3-5)"),
        make_tuple(6, "test alias(6-8)"),
        make_tuple(7, "test alias(6-8)"),
        make_tuple(8, "test alias(6-8)"),
        make_tuple(9, "test alias(9-11)"),
      };

      TNamed histogram;
      HistogramDef histogramDef;
      histogramDef.alias = "test alias";
      histogramDef.opt = "kLBNHistoryDepth=3";

      LumiblockHistogramProvider testObj(m_gmTool.get(), m_histogramFactory, histogramDef);

      for (auto input : expectedFlow) {
        const unsigned lumiBlock = get<0>(input);
        const string expectedAlias = get<1>(input);

        m_gmTool->mock_lumiBlock = [lumiBlock]() { return lumiBlock; };
        m_histogramFactory->mock_create = [&histogram, expectedAlias](const HistogramDef& def) mutable {
          VALUE(def.alias) EXPECTED(expectedAlias);
          return &histogram;
        };

        TNamed* const result = testObj.histogram();
        VALUE(result) EXPECTED(&histogram);
      }
    }

    void test_shouldCreateNewHistogramWithUpdatedLumiBlock() {
      auto expectedFlow = {
        make_tuple(100, 100000, "/run_100000/lowStat101-120/"),
        make_tuple(125, 200000, "/run_200000/lowStat121-140/"),
      };

      TNamed histogram;
      HistogramDef histogramDef;
      histogramDef.convention = "OFFLINE:lowStat";

      OfflineHistogramProvider testObj(m_gmTool.get(), m_histogramFactory, histogramDef);

      for (auto input : expectedFlow) {
        const unsigned lumiBlock = get<0>(input);
        const unsigned runNumber = get<1>(input);
        const string expectedTld = get<2>(input);

        m_gmTool->mock_lumiBlock = [lumiBlock]() { return lumiBlock; };
        m_gmTool->mock_runNumber = [runNumber]() { return runNumber; };
        m_histogramFactory->mock_create = [&histogram, expectedTld](const HistogramDef& def) mutable {
          VALUE(def.tld) EXPECTED(expectedTld);
          return &histogram;
        };

        TNamed* const result = testObj.histogram();
        VALUE(result) EXPECTED(&histogram);
      }
    }

  // ==================== Helper methods ====================
  private:

  // ==================== Initialization & run ====================
  public:
    LumiblockHistogramProviderTestSuite() 
      : m_log(Athena::getMessageSvc(), "LumiblockHistogramProviderTestSuite") {
    }

    void run() {
      for (function<void(void)> testCase : registeredTestCases()) {
        testCase();
      }
    }

  // ==================== Test case registration ====================
  private:
    typedef void (LumiblockHistogramProviderTestSuite::*TestCase)(void);

    function<void(void)> registerTestCase(TestCase testCase, string testCaseName) {
      return [this, testCase, testCaseName]() {
        m_log << MSG::INFO << "Current test case: " << testCaseName << endmsg;
        beforeEach();
        invoke(testCase, this);
        afterEach();
      };
    }

  // ==================== Properties ====================
  private:
    MsgStream m_log;

    shared_ptr<MockGenericMonitoringTool> m_gmTool;
    shared_ptr<MockHistogramFactory> m_histogramFactory;
};

int main() {
  ISvcLocator* pSvcLoc;

  if (!Athena_test::initGaudi("GenericMon.txt", pSvcLoc)) {
    throw runtime_error("This test can not be run: GenericMon.txt is missing");
  }

  LumiblockHistogramProviderTestSuite().run();

  return 0;
}
